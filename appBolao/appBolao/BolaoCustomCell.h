//
//  CustomCell.h
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BolaoCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *homeTeamLabel;
@property (weak, nonatomic) IBOutlet UILabel *awayTeamLabel;
@property (weak, nonatomic) IBOutlet UILabel *betLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *predictLabel;

@property (weak, nonatomic) IBOutlet UIImageView *winnerImage;
@property (weak, nonatomic) IBOutlet UIImageView *loserImage;

@property (weak, nonatomic) IBOutlet UIImageView *smallHomeImage;
@property (weak, nonatomic) IBOutlet UIImageView *smallAwayImage;

@end
