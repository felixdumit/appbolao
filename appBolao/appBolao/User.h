//
//  User.h
//  appBolao
//
//  Created by Rafael Padilha on 27/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bet.h"
#import "Game.h"

@interface User : NSObject <NSCoding>
@property (nonatomic,strong) NSString* userName;
@property (nonatomic,strong) NSString* userId;
@property (nonatomic,strong) NSString* password;


@property (nonatomic) NSInteger betPoints;
@property (nonatomic) NSInteger numBets;
@property (nonatomic) NSInteger totalBetPoints;
@property (nonatomic) NSInteger totalWinnings;



@property (nonatomic,strong) NSMutableArray* betArray;

//-(User*) initWithDictionary:(NSDictionary*) dict;
-(void) updateWithDictionary:(NSDictionary*) dict;



+ (User *)sharedUser;


//- (User*) initWithUserName:(NSString*) userName betPoints:(NSInteger)betPoints bolaoArray:(NSMutableArray*)bolaoArray;
//- (User*) initWithUserId:(NSString*) userId withName:(NSString*) userName andPassword:(NSString*) password;

-(Bet*) getUserBetOnGame:(Game*) game;

- (BOOL) hasBetOnGame:(Game*) game;

- (void) removeBetOnGame:(Game*) game;

-(void) betInGame:(Game*) game withBet:(NSInteger) betValue onTeam:(NSString*) betTeam;

-(NSInteger) predictWinningOfBet:(Bet*) bet onGame:(Game*)game;

+(void) archiveUser;

+(User*) loadArchivedUser;

+(void) logoutUser;




@end
