//
//  BolaoDetailsVC.m
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "GameDetailsVC.h"
#import "UIImageView+FromURL.h"

@interface GameDetailsVC (){
    Bet* previousBet;
    Bet* userBet;
}
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@end

@implementation GameDetailsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // load shared user
    self.user = [User sharedUser];
    
    
    self.homeLabel.text = self.game.homeTeam;
    self.awayLabel.text = self.game.awayTeam;
    self.totalPrizeLabel.text = [NSString stringWithFormat:@"Total BetPoints: %ld",(long)self.game.totalPoints];
    self.partLabel.text = [NSString stringWithFormat:@"Number of participants: %ld", (long)self.game.numBets];
    
    
    [self.homeImageView setImageFromURL:self.game.homeImageURL];
    [self.awayImageView setImageFromURL:self.game.awayImageURL];
    
    
    // get previous bet if user has already bet on game
    previousBet = [self.user getUserBetOnGame:self.game];
    // create new bet
    userBet = [[Bet alloc] initWithUserName:self.user.userName andGameId:self.game.gameId];
    
    // set new bet parameters equal to previous one if it exists
    self.removeBetButton.hidden = YES;
    if(previousBet){
        userBet.betValue = previousBet.betValue;
        userBet.predict = previousBet.predict;
        // dont hide removebutton if user has a bet on game
        self.removeBetButton.hidden = NO;
    }
    
    self.userBetTextField.text = [NSString stringWithFormat:@"%ld", (long)userBet.betValue];
    
    // set segmented control according to the team the user predicts
    if ([userBet.predict isEqualToString:@"H"]) self.teamBetSegControl.selectedSegmentIndex = 0;
    else if ([userBet.predict isEqualToString:@"D"]) self.teamBetSegControl.selectedSegmentIndex = 1;
    else self.teamBetSegControl.selectedSegmentIndex = 2;
    
    
    [self updateUserBet];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    self.user = [User sharedUser];
    self.topLabel.text = [NSString stringWithFormat:@"%@, you have %ld betpoints", self.user.userName, (long)self.user.betPoints];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segControlChanged:(id)sender {
    
    [self updateUserBet];
}

- (IBAction)betValueChanged:(id)sender {
    [self updateUserBet];
}


// update prediction of user bet based on predicted team and number of points added
-(void) updateUserBet
{
    NSInteger teamBet = [self.teamBetSegControl selectedSegmentIndex];
    if (teamBet == 0) userBet.predict = @"H";
    else if (teamBet == 1) userBet.predict = @"D";
    else userBet.predict = @"A";
    
    userBet.betValue = [self.userBetTextField.text integerValue];
    
    self.willWinLabel.text = [NSString stringWithFormat:@"You will win: %ld betpoints",
                              (long)[self.user predictWinningOfBet:userBet onGame:self.game]];
}



- (IBAction)saveButton:(id)sender {
    
    // call user to bet in game
    [self.user betInGame:self.game
                 withBet:[self.userBetTextField.text integerValue]
                  onTeam:[@[@"H",@"D",@"A"] objectAtIndex:[self.teamBetSegControl selectedSegmentIndex]]
     ];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)removeBetButton:(id)sender{
    
    // call user to remove bet in game
    [self.user removeBetOnGame:self.game];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (IBAction)startedEnteringBet:(id)sender {
    // clear field before user types
    self.userBetTextField.text = @"";
}

- (IBAction)finishedEnteringBet:(id)sender {
    // if user doesnt have enough betpoints
    if (self.user.betPoints < [self.userBetTextField.text integerValue]) {
        [self.saveButton setTitle:@"You don't have enough BetPoints" forState:UIControlStateNormal];
        self.saveButton.enabled = NO;
    }
    // sufficient betpoints
    else{
        [self.saveButton setTitle:@"Save" forState:UIControlStateNormal];
        self.saveButton.enabled = YES;
    }
}


-(void)dismissKeyboard
{
    [self.userBetTextField resignFirstResponder];
    
}
@end
