//
//  BolaoDetailsVC.h
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bet.h"
#import "User.h"

@interface GameDetailsVC : UIViewController
@property (nonatomic,strong) Game *game;
@property (nonatomic,strong) User *user;

@property (weak, nonatomic) IBOutlet UIImageView *homeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *awayImageView;
@property (weak, nonatomic) IBOutlet UILabel *homeLabel;
@property (weak, nonatomic) IBOutlet UILabel *awayLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPrizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *partLabel;
@property (weak, nonatomic) IBOutlet UITextField *userBetTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *teamBetSegControl;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *removeBetButton;
@property (weak, nonatomic) IBOutlet UILabel *willWinLabel;

@end
