//
//  TitleScreenViewController.m
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "TitleScreenVC.h"
#import "RBRequester.h"

#import <MBProgressHUD/MBProgressHUD.h>



@interface TitleScreenVC (){
    NSString* username;
    NSString* password;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation TitleScreenVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    //load user from userdefaults
    User* user = [User loadArchivedUser];
    username = user.userName;
    password = user.password;
    
    // update form to show user data if it is stored
    self.usernameField.text = username;
    self.passwordField.text = password;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)enterButton:(id)sender {
    
    void (^successBlock)(AFHTTPRequestOperation *operation, NSDictionary* responseObject) = ^void(AFHTTPRequestOperation *operation, NSDictionary* json){
        [self.activityIndicator stopAnimating];
        
        // update user after login
        User *user = [User sharedUser];
        [user updateWithDictionary:json];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [self performSegueWithIdentifier:@"titleScreenSegue" sender:nil];
        
        
    };
    void (^errorBlock)(AFHTTPRequestOperation *operation, NSError* error) = ^void(AFHTTPRequestOperation *operation, NSError* error){
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if(operation.responseData == NULL){
            // user has entered wrong password
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"No connection with server"
                                                              message:@"Check your connection, or server unavailable"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
        }
        
        else {
        // user has entered wrong password
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Incorrect password"
                                                          message:@"register new user or change password"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        }
    };
    
    
    
    if (self.usernameField.hidden == NO) {
        username = self.usernameField.text;
        password = self.passwordField.text;
    }
    
    
    // no username entered
    if([username isEqualToString:@""]){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please enter username"
                                                          message:@"enter your username or a new one to register"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    }
    // no password entered
    else if([password isEqualToString:@""]){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Please enter password"
                                                          message:@"password can't be blank"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    }
    // everything entered correctly
    else{
        
        [[RBRequester sharedRequester] setLogin:username andPassword:password];
        [[RBRequester sharedRequester] requestWithURL:@"/login" withParams:@{} onSuccess:successBlock
                                      onFailureailure:errorBlock
                                        duringRequest:Nil];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
    
    
    
}

// display instructions to login/register
- (IBAction)clickInfo:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Register or login"
                                                      message:@"login with a created user or simply enter a new username and password to register"
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}


- (IBAction)showAbout:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Welcome to FutBolao"
                                                      message:@"FutBolao is a fun app for predicting football(soccer) games,users can bet points on the results of matches, the total prize of a game is split among the winners, according to their bet.\
                            Developed by Felix Dumit and Rafael Padilha, all data comes from goal.com"
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    
}

// Method to dismiss the keyboard
-(void)dismissKeyboard
{
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}

@end
