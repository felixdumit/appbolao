//
//  ViewController.m
//  appBolao
//
//  Created by Rafael Padilha on 27/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "MeVC.h"
#import "UIImageView+FromURL.h"

@interface MeVC ()
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *betPointsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userAvatarIV;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *totalBets;
@property (weak, nonatomic) IBOutlet UILabel *totalWinnings;
@property (weak, nonatomic) IBOutlet UILabel *accumulatedBetValue;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property (nonatomic,strong) User* user;
@end

@implementation MeVC

- (void) viewWillAppear:(BOOL)animated{

    // get shared user
    User* user = [User sharedUser];
    
    // set tolabel to show betpoints
    self.topLabel.text = [NSString stringWithFormat:@"%@, you have %ld betpoints", user.userName, (long)user.betPoints];
    
    
    //set user stats
    self.navigationItem.title = user.userName;
    self.userNameLabel.text = user.userName;
    self.betPointsLabel.text = [NSString stringWithFormat:@"%ld BetPoints",(long)user.betPoints];
    self.totalBets.text = [NSString stringWithFormat:@"Total Bets: %ld", (long)user.numBets];
    self.accumulatedBetValue.text = [NSString stringWithFormat:@"Accumulated BetValue: %ld", (long)user.totalBetPoints];
    self.totalWinnings.text = [NSString stringWithFormat:@"Total Winnings: %ld", (long)user.totalWinnings];
    
    [self.userImage setImage:[UIImage imageNamed:@"defaultUser"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickLogout:(id)sender {
    
    NSLog(@"logout");
    
    [User logoutUser];
    [self.tabBarController dismissViewControllerAnimated:YES completion:nil];
    
    //[self performSegueWithIdentifier:@"backToLogin" sender:self];
    
}

- (IBAction)buyBetPoints:(id)sender {
   
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Coming soon"
                                                      message:@"come back soon to buy betpoints!"
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

@end
