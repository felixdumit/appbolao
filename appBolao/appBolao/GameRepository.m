//
//  GameRepository.m
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "GameRepository.h"
#import "RBRequester.h"


@implementation GameRepository
static GameRepository* _gameRepository = nil;


+(id)allocWithZone:(struct _NSZone *)zone
{
    return [GameRepository sharedRepository];
}

-(id)init
{
    self = [super init];
    
    if (self){
        self.allGames = [[NSMutableDictionary alloc] init];
        
        
    }
    return self;
}

#pragma mark - Singleton methods

+(GameRepository *)sharedRepository
{
    if (!_gameRepository) {
        _gameRepository= [[super allocWithZone:nil] init];
    }
    
    return _gameRepository;
}

-(void) downloadGamesForDay:(NSString*)day
{
    
    void (^successBlock)(AFHTTPRequestOperation *operation, NSDictionary* responseObject) = ^void(AFHTTPRequestOperation *operation, NSDictionary* json){
        
        NSMutableDictionary* newGames = [[NSMutableDictionary alloc] init];
        
        // load games grouped by league
        NSArray* leagues = [json allKeys];
        for(NSString* league in leagues){
            NSArray* games = [json objectForKey:league];
            NSMutableArray* temp = [[NSMutableArray alloc] init];
            for(NSDictionary* game in games){
                [temp addObject:[[Game alloc] initWithDictionary:game]];
            }
            [newGames setObject:temp forKey:league];
        }
        
        self.allGames = newGames;
        
        [self saveGamesForDay:day];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadedGames" object:self];
        
    };
    
    
    [[RBRequester sharedRequester] requestWithURL:@"/games/search" withParams:@{@"day": day}
                                        onSuccess:successBlock onFailureailure:nil duringRequest:Nil];
}




-(NSInteger) numberOfLeagues
{
    return [[self.allGames allKeys] count];
}

-(NSArray*) gamesForLeague:(NSString*) league
{
    return [self.allGames objectForKey:league];
}

-(Game*) getGameForGameId:(NSString*) gameId
{
    for(NSString* league in [self.allGames allKeys]){
        
        for(Game* g in [self.allGames objectForKey:league]){
            if([g.gameId isEqualToString:gameId]){
                return g;
            }
        }
    }
    
    return Nil;
}


// get games a user has bet on
-(void) fetchGamesFromUserBet:(User*) user
{
    void (^successBlock)(AFHTTPRequestOperation *operation, NSDictionary* responseObject) = ^void(AFHTTPRequestOperation *operation, NSDictionary* json){
        
        NSMutableArray* temp = [[NSMutableArray alloc] init];
        for(NSDictionary* game in [json objectForKey:@"games"]){
            [temp addObject:[[Game alloc] initWithDictionary:game]];
        }
        
        self.betGames = temp;
        
        //        [self saveBetGames];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadedBetGames" object:self];
        
    };
    
    
    
    [[RBRequester sharedRequester] requestWithURL:@"/users/gameBets" withParams:@{}
                                        onSuccess:successBlock onFailureailure:nil duringRequest:Nil];
    
    
}




-(void) restoreGamesForDay:(NSString*) day
{
    self.allGames = [[NSMutableDictionary alloc] init];
    NSData * data = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"games%@", day]];
    self.allGames = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

-(void) saveGamesForDay:(NSString*) day
{
    NSData* data = [NSKeyedArchiver archivedDataWithRootObject:self.allGames];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:[NSString stringWithFormat:@"games%@", day]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}





@end