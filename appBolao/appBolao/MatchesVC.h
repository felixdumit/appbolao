//
//  MatchesVC.h
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameRepository.h"
#import "MatchesCustomCell.h"
#import "GameDetailsVC.h"

@interface MatchesVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
