//
//  UIImage+FromURL.h
//  
//
//  Created by Felix Dumit on 2/3/14.
//
//

#import <UIKit/UIKit.h>

@interface UIImageView (FromURL)


-(void) setImageFromURL:(NSString*) url withPlaceholder:(UIImage*) placeholder;


-(void) setImageFromURL:(NSString*) url;


@end
