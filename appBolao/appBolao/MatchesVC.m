//
//  MatchesVC.m
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "UIImageView+FromURL.h"
#import "MatchesVC.h"
#import "UIColor+Named.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface MatchesVC ()<UITableViewDelegate, UITableViewDataSource>{
    Game* selectedGame;
    User* user;
}


@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *daySelector;
@property (strong, nonatomic) UIRefreshControl* refreshControl;
@end

@implementation MatchesVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set refresh control for table view
    self.refreshControl = [[UIRefreshControl alloc]  init];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Release to Refresh"];
    self.refreshControl.tintColor = [UIColor darkGrayColor];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(downloadGames) forControlEvents:UIControlEventValueChanged];
    
    
    
    // listen for notification to reload table (changed data)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changedGames)
                                                 name:@"loadedGames" object:nil];
    
    // listen fro user change
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changedUser)
                                                 name:@"changedUser" object:nil];
    
    
    [self downloadGames];
    
    
}

-(void) changedUser
{
    // set top label
    user = [User sharedUser];
    self.topLabel.text = [NSString stringWithFormat:@"%@, you have %ld betpoints", user.userName, (long)user.betPoints];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [self changedUser];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// download games has finished, reload table
-(void) changedGames
{
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


-(NSString*) getSelectedDay
{
    return [@[@"yesterday", @"today", @"tomorrow"] objectAtIndex:self.daySelector.selectedSegmentIndex];
}


// restore game from user defaults
-(void) restoreGames
{
    [[GameRepository sharedRepository] restoreGamesForDay:[self getSelectedDay]];
    [self.tableView reloadData];
    
    // if no games, load from server
    if([[GameRepository sharedRepository] numberOfLeagues] ==0){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self downloadGames];
        
    }
    
}

// download games from server
-(void) downloadGames
{
    [[GameRepository sharedRepository] downloadGamesForDay:[self getSelectedDay]];
    [self.refreshControl beginRefreshing];
    
    MBProgressHUD* HUD = [[MBProgressHUD alloc]  initWithView:self.view ];
    [self.view addSubview:HUD];
    HUD.labelText = @"loading games from server";
    [HUD show:YES];
    
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"updating games .."];
}

- (IBAction)dayChanged:(id)sender {
    
    [self restoreGames];
}


- (IBAction)clickRefresh:(id)sender {
    [self downloadGames];
}

#pragma mark - Table view data source


- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
    // set alternating row colors
    if(indexPath.row % 2){
        cell.backgroundColor = [UIColor colorFromName:@"Alice blue"];
    }
    else{
        cell.backgroundColor = [UIColor colorFromName:@"Antique white"];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // number of gaes for each league
    NSString* key = [[[[GameRepository sharedRepository] allGames] allKeys] objectAtIndex:section];
    return [[[GameRepository sharedRepository] gamesForLeague:key] count];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // number of leagues
    return [[GameRepository sharedRepository] numberOfLeagues];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    // league name
    NSString* key = [[[[GameRepository sharedRepository] allGames] allKeys] objectAtIndex:section];
    return key;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    /* Section header is in 0th index... */
    [label setText:[self tableView:tableView titleForHeaderInSection:section]];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0f;
}



- (MatchesCustomCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MatchesCustomCell *cell = (MatchesCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"matchesCell"];
    
    GameRepository *repository = [GameRepository sharedRepository];
    
    NSString* key = [[[repository allGames] allKeys] objectAtIndex:indexPath.section];
    
    NSArray* games = [[repository allGames] objectForKey:key];
    Game* game = [games objectAtIndex:indexPath.row];
    
    //Set the cell's properties and background color
    cell.homeLabel.text = game.homeTeam;
    cell.awayLabel.text = game.awayTeam;
    
    // if game has score how it, else show 'vs'
    if ([game.score isEqualToString:@""]){
        cell.scoreLabel.text = @" vs ";
    }
    else {
        cell.scoreLabel.text = game.score;
    }
    
    // if game has not started, show start time and allow disclosure
    if([game.status isEqualToString:@""]){
        cell.statusLabel.text = game.time;
        cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
        
    }
    // else show game status
    else{
        cell.statusLabel.text = game.status;
        cell.accessoryType =  UITableViewCellAccessoryNone;
        
    }
    
    // set logos
    [cell.homeImage setImageFromURL:game.homeImageURL];
    [cell.awayImage setImageFromURL:game.awayImageURL];
    
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set selected game
    NSString* key = [[[[GameRepository sharedRepository] allGames] allKeys] objectAtIndex:indexPath.section];
    NSArray* games = [[[GameRepository sharedRepository] allGames] objectForKey:key];
    selectedGame = [games objectAtIndex:indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if([selectedGame.status isEqualToString:@""]){
        [self performSegueWithIdentifier:@"showSelectedGame" sender:self];
    }
    
    
}





#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showSelectedGame"])
    {
        GameDetailsVC *vc = [segue destinationViewController];
        
        vc.game = selectedGame;
        
    }
}


@end
