//
//  GameRepository.h
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"
#import "User.h"

@interface GameRepository : NSObject
@property (nonatomic,strong) NSMutableDictionary *allGames;
@property (nonatomic,strong) NSMutableArray *betGames;


+ (GameRepository*) sharedRepository;
-(void) downloadGamesForDay:(NSString*)day;
-(NSInteger) numberOfLeagues;
-(NSArray*) gamesForLeague:(NSString*) league;

-(Game*) getGameForGameId:(NSString*) gameId;

-(void) fetchGamesFromUserBet:(User*) user;



-(void) restoreGamesForDay:(NSString*) day;
-(void) saveGamesForDay:(NSString*) day;
//-(void) restoreBetGames;
//-(void) saveBetGames;

@end
