//
//  Game.h
//  appBolao
//
//  Created by Rafael Padilha on 27/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bet.h"


@interface Game : NSObject <NSCoding>
@property (nonatomic,strong) NSString* homeTeam;
@property (nonatomic,strong) NSString* awayTeam;
@property (nonatomic,strong) NSString* date;
@property (nonatomic, strong) NSString* time;
@property (nonatomic, strong) NSString *score;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString* gameId;
@property (nonatomic, strong) NSString* league;
@property (nonatomic, strong) NSString* homeImageURL;
@property (nonatomic, strong) NSString* awayImageURL;
@property (nonatomic) NSInteger numBets;
@property (nonatomic) NSInteger totalPoints;
@property (nonatomic) NSInteger totalA;
@property (nonatomic) NSInteger totalH;
@property (nonatomic) NSInteger totalD;
@property (nonatomic, strong) NSMutableArray* betArray;





- (Game*) initWithDictionary:(NSDictionary*) dict;

-(void) updateWithDictionary:(NSDictionary*) dict;

@end