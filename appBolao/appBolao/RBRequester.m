//
//  RBRequester.m
//  appBolao
//
//  Created by Felix Dumit on 1/31/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "RBRequester.h"

@interface RBRequester(){
    
}

@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;

@end

@implementation RBRequester

static RBRequester *_requester = nil;


- (id)init
{
    self = [super init];
    if (self) {
        self.manager = [[AFHTTPRequestOperationManager alloc]
                        initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        
        NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024
                                                             diskCapacity:20 * 1024 * 1024
                                                                 diskPath:nil];
        [NSURLCache setSharedURLCache:URLCache];
        
    }
    return self;
}


+(RBRequester *)sharedRequester
{
    if (!_requester) {
        _requester= [[super alloc] init];
    }
    
    return _requester;
}


// set login credentials based on username and password
-(void) setLogin:(NSString*)user andPassword:(NSString*) password
{
    
    [self.manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    
    [self.manager.requestSerializer setAuthorizationHeaderFieldWithUsername:user password:password];
    
    [self.manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
}


// request with url, params, and success, error, and after blocks
-(void) requestWithURL:(NSString*) url withParams:(NSDictionary*) params
             onSuccess:(void(^)(AFHTTPRequestOperation *operation, NSDictionary* responseObject)) successBlock
       onFailureailure:(void(^)(AFHTTPRequestOperation *operation, NSError *error)) failureBlock
         duringRequest:(void(^)()) afterBlock;
{
    
    
    [self.manager GET:url parameters:params success:successBlock failure:failureBlock];
    if(afterBlock)  afterBlock();
}

@end
