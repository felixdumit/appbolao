//
//  Bolao.h
//  appBolao
//
//  Created by Rafael Padilha on 27/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bet : NSObject <NSCoding>
@property (nonatomic) NSInteger betValue;
@property (nonatomic,strong) NSString *predict;
@property (nonatomic,strong) NSString* gameId;
@property (nonatomic,strong) NSString* username;
@property (nonatomic) NSInteger winnings;

@property (nonatomic) BOOL completed;

- (Bet *) initWithDictionary:(NSDictionary*) dict;

- (Bet *)initWithUserName:(NSString*)username andGameId:(NSString*) gameId;


@end
