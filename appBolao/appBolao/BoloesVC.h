//
//  BoloesVC.h
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Bet.h"
#import "BolaoCustomCell.h"
#import "GameDetailsVC.h"

@interface BoloesVC : UIViewController

@end
