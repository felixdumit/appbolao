//
//  Bolao.m
//  appBolao
//
//  Created by Rafael Padilha on 27/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "Bet.h"

@implementation Bet


- (Bet *)initWithUserName:(NSString*)username andGameId:(NSString*) gameId
{
    self = [super init];
    if (self) {
        self.gameId = gameId;
        self.username = username;
        self.betValue = 0;
        self.completed = NO;
        self.predict = @"";
        self.winnings = 0;
    }
    return self;
}

- (Bet *) initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.gameId = [dict objectForKey:@"gameId"];
        self.betValue = [[dict objectForKey:@"betValue"] integerValue];
        self.completed = [[dict objectForKey:@"completed"] boolValue];
        self.predict = [dict objectForKey:@"predict"];
        self.winnings =[[dict objectForKey:@"winnings"] integerValue];
    }
    return self;
}

#pragma mark - NSCoding Protocol

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.gameId forKey:@"gameId"];
    [encoder encodeInteger:self.betValue forKey:@"betValue"];
    [encoder encodeBool:self.completed forKey:@"completed"];
    [encoder encodeObject:self.predict forKey:@"predict"];
    [encoder encodeInteger:self.winnings forKey:@"winnings"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if(self = [super init]){
        self.gameId = [decoder decodeObjectForKey:@"gameId"];
        self.betValue = [decoder decodeIntegerForKey:@"betValue"];
        self.completed = [decoder decodeBoolForKey:@"completed"];
        self.predict = [decoder decodeObjectForKey:@"predict"];
        self.winnings = [decoder decodeIntegerForKey:@"winnings"];
    }
    return self;
}

@end