//
//  Game.m
//  appBolao
//
//  Created by Rafael Padilha on 27/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "Game.h"

@implementation Game

-(Game*) initWithDictionary:(NSDictionary*) dict
{
    if (self = [super init]) {
        [self updateWithDictionary:dict];
    }
    return self;
}


-(void) updateWithDictionary:(NSDictionary*) dict
{
    self.homeTeam = [dict objectForKey:@"Home"];
    self.awayTeam = [dict objectForKey:@"Away"];
    self.league = [dict objectForKey:@"League"];
    self.date = [dict objectForKey:@"date"];
    self.time = [dict objectForKey:@"time"];
    self.status = [dict objectForKey:@"status"];
    self.score = [dict objectForKey:@"score"];
    self.gameId = [dict objectForKey:@"_id"];
    self.homeImageURL = [dict objectForKey:@"HomeLogo"];
    self.awayImageURL = [dict objectForKey:@"AwayLogo"];
    self.numBets = [[dict objectForKey:@"numBets"] integerValue];
    self.totalPoints = [[dict objectForKey:@"total"] integerValue];
    self.totalA = [[dict objectForKey:@"totalA"] integerValue];
    self.totalH = [[dict objectForKey:@"totalH"] integerValue];
    self.totalD = [[dict objectForKey:@"totalD"] integerValue];
    
    self.betArray = [[NSMutableArray alloc] init];
    
    for(NSDictionary* bet in [dict objectForKey:@"bets"]){
        Bet* b = [[Bet alloc] initWithDictionary:bet];
        b.username = [bet objectForKey:@"user"];
        b.gameId = self.gameId;
        [self.betArray addObject:b];
    }
    
    
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.homeTeam forKey:@"homeTeam"];
    [encoder encodeObject:self.awayTeam forKey:@"awayTeam"];
    [encoder encodeObject:self.date forKey:@"date"];
    [encoder encodeObject:self.league forKey:@"league"];
    [encoder encodeObject:self.time forKey:@"time"];
    [encoder encodeObject:self.status forKey:@"status"];
    [encoder encodeObject:self.score forKey:@"score"];
    [encoder encodeObject:self.gameId forKey:@"gameId"];
    [encoder encodeObject:self.homeImageURL forKey:@"homeImageURL"];
    [encoder encodeObject:self.awayImageURL forKey:@"awayImageURL"];
    
    [encoder encodeInteger:self.numBets forKey:@"numBets"];
    [encoder encodeInteger:self.totalPoints forKey:@"totalPoints"];
    [encoder encodeInteger:self.totalA forKey:@"totalA"];
    [encoder encodeInteger:self.totalH forKey:@"totalH"];
    [encoder encodeInteger:self.totalD forKey:@"totalD"];
    
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if(self = [super init]){
        self.homeTeam = [decoder decodeObjectForKey:@"homeTeam"];
        self.awayTeam = [decoder decodeObjectForKey:@"awayTeam"];
        self.date = [decoder decodeObjectForKey:@"date"];
        self.league = [decoder decodeObjectForKey:@"league"];
        self.time = [decoder decodeObjectForKey:@"time"];
        self.status = [decoder decodeObjectForKey:@"status"];
        self.score = [decoder decodeObjectForKey:@"score"];
        self.gameId = [decoder decodeObjectForKey:@"gameId"];
        self.homeImageURL = [decoder decodeObjectForKey:@"homeImageURL"];
        self.awayImageURL = [decoder decodeObjectForKey:@"awayImageURL"];
        
        self.numBets = [decoder decodeIntegerForKey:@"numBets"];
        self.totalPoints = [decoder decodeIntegerForKey:@"totalPoints"];
        self.totalA = [decoder decodeIntegerForKey:@"totalA"];
        self.totalH = [decoder decodeIntegerForKey:@"totalH"];
        self.totalD = [decoder decodeIntegerForKey:@"totalD"];
        
        
    }
    return self;
}

@end