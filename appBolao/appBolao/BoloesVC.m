//
//  BoloesVC.m
//  appBolao
//
//  Created by Rafael Padilha on 28/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "BoloesVC.h"
#import "GameRepository.h"
#import "UIImageView+FromURL.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIColor+Named.h"


@interface BoloesVC (){
    Game* selectedGame;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) User *user;
@property (strong, nonatomic) UIRefreshControl* refreshControl;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segControl;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@end


@implementation BoloesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    
    // listen for notification to reload table (changed data)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadedBetGames)
                                                 name:@"loadedBetGames" object:nil];
    
    // listen to check if a bet was removed
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadBetGames)
                                                 name:@"removeBetInGame" object:nil];
    
    // listen to check if a new bet was created
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadBetGames)
                                                 name:@"addBetInGame" object:nil];
    
    // listen for user change
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changedUser)
                                                 name:@"changedUser" object:nil];
    
    
    // create refresh control for table view
    self.refreshControl = [[UIRefreshControl alloc]  init];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Release to Refresh"];
    self.refreshControl.tintColor = [UIColor darkGrayColor];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(loadBetGames) forControlEvents:UIControlEventValueChanged];
    
    [self loadBetGames];
    
    
    [super viewDidLoad];
    
}


-(void) changedUser
{
    self.user = [User sharedUser];
    
    self.topLabel.text = [NSString stringWithFormat:@"%@, you have %ld betpoints", self.user.userName, (long)self.user.betPoints];
    
    [self loadBetGames];
    
}

- (void) viewWillAppear:(BOOL)animated{
    
    [self changedUser];
    
    
}

// user changed the category of bets viewed (open, finished, all)
- (IBAction)segControlChanged:(id)sender {
    
    [self.tableView reloadData];
}

//-(void) restoreBetGames
//{
//    [[GameRepository sharedRepository] restoreBetGames];
//
//    if([[[GameRepository sharedRepository] betGames] count] == 0){
//        [self loadBetGames];
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    }
//
//    [self.tableView reloadData];
//
//}

// load betgames from server
-(void) loadBetGames
{
    MBProgressHUD* HUD = [[MBProgressHUD alloc]  initWithView:self.view ];
    [self.view addSubview:HUD];
    HUD.labelText = @"loading bets from server";
    [HUD show:YES];    [self.refreshControl beginRefreshing];
    [[GameRepository sharedRepository] fetchGamesFromUserBet:self.user];
    
}

// finished loading bets from server
-(void) loadedBetGames
{
    [self.tableView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.refreshControl endRefreshing];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickRefresh:(id)sender {
    [self loadBetGames];
}


// return bets array based on selected category (open, finished, all)
-(NSArray*) getArrayToDisplay
{
    NSArray* betGames = [[GameRepository sharedRepository] betGames];
    NSPredicate* pred = Nil;
    if(self.segControl.selectedSegmentIndex ==0){
        pred = [NSPredicate predicateWithFormat:@"status = ''"];
    }
    else if(self.segControl.selectedSegmentIndex ==1){
        pred = [NSPredicate predicateWithFormat:@"status = 'Full time'"];
    }
    else{
        return betGames;
    }
    
    return [betGames filteredArrayUsingPredicate:pred];
}

-(NSDictionary*) getGamesByDate
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    NSArray* array = [self getArrayToDisplay];
    for(Game* game in array){
        if([dict objectForKey:game.date] == nil) [dict setObject:[[NSMutableArray alloc] initWithObjects:game, nil] forKey:game.date];
        else [[dict objectForKey:game.date] addObject:game];
    }

    return dict;
}

#pragma mark - Table view data source

- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
    // set alternating row colors
    if(indexPath.row % 2){
        cell.backgroundColor = [UIColor colorFromName:@"Alice blue"];
    }
    else{
        cell.backgroundColor = [UIColor colorFromName:@"Antique white"];
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary * gms = [self getGamesByDate];
    NSArray* keys = [[[[gms allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] reverseObjectEnumerator] allObjects];
    
    return [[gms objectForKey:[keys objectAtIndex:section]] count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSDictionary * gms = [self getGamesByDate];
    return [[gms allKeys] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    // league name
    NSDictionary * gms = [self getGamesByDate];
    NSArray* keys = [[[[gms allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] reverseObjectEnumerator] allObjects];

    return [keys objectAtIndex:section];
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    /* Section header is in 0th index... */
    [label setText:[self tableView:tableView titleForHeaderInSection:section]];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0f;
}


- (BolaoCustomCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BolaoCustomCell *cell = (BolaoCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary * gms = [self getGamesByDate];
    NSArray* keys = [[[[gms allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] reverseObjectEnumerator] allObjects];

    NSString* date = [keys objectAtIndex:indexPath.section];
    Game* game = [[gms objectForKey:date] objectAtIndex:indexPath.row];

    
//    Game *game = [[self getArrayToDisplay] objectAtIndex:indexPath.row];
    Bet* bet;
    // get bet corresponding to game
    for(Bet* b in self.user.betArray){
        if([b.gameId isEqualToString:game.gameId]) {
            bet = b;
            break;
        };
    }
    
    // set home and away teams and betpoints
    cell.homeTeamLabel.text = game.homeTeam;
    cell.awayTeamLabel.text = game.awayTeam;
    
    cell.betLabel.text = [NSString stringWithFormat:@"%ld betpoints",(long)bet.betValue];
    
   
    // if there is a score, display it, else show 'vs'
    if ([game.score isEqualToString:@""]){
        cell.scoreLabel.text = @" vs ";
    }
    else {
        cell.scoreLabel.text = game.score;
    }
    
    // set logos
    [cell.smallHomeImage setImageFromURL:game.homeImageURL];
    [cell.smallAwayImage setImageFromURL:game.awayImageURL];
    
    // set images for prediction
    if([bet.predict isEqualToString:@"H"]){
        [cell.winnerImage setImageFromURL: game.homeImageURL];
        cell.loserImage.image = Nil;
        cell.predictLabel.text = @"win";
    }
    else if([bet.predict isEqualToString:@"A"]){
        [cell.winnerImage setImageFromURL: game.awayImageURL];
        cell.loserImage.image = Nil;
        cell.predictLabel.text = @"win";
    }
    else {
        [cell.winnerImage setImageFromURL: game.homeImageURL];
        [cell.loserImage setImageFromURL:game.awayImageURL];
        cell.predictLabel.text = @"draw";
    }
    
    // if game is finished show winnings
    if(bet.completed){
        NSInteger win = bet.winnings + bet.betValue;
        cell.resultLabel.text = [NSString stringWithFormat:@"%@%ld BPs",
                                 (win >= 0 ? @"+" : @"-"),  (long)win];
        
        // show checkmark to indicate game is over
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        
        // if game hasnt started, show start time and disclosure indicator (user can edit bet)
        if([game.status isEqualToString:@""]){
            cell.resultLabel.text = game.time;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        // game is underway
        else {
            cell.resultLabel.text = [NSString stringWithFormat:@"Playing: %@",game.status];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // set selected game
    NSDictionary * gms = [self getGamesByDate];
    NSArray* keys = [[[[gms allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] reverseObjectEnumerator] allObjects];
    
    NSString* date = [keys objectAtIndex:indexPath.section];
    selectedGame = [[gms objectForKey:date] objectAtIndex:indexPath.row];
    
    // deselect row
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // if game has not started, go to details screen
    if([selectedGame.status isEqualToString:@""]){
        [self performSegueWithIdentifier:@"bolaoDetailsSegue" sender:self];
    }
    
    
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"bolaoDetailsSegue"])
    {
        // pass selected game
        GameDetailsVC *vc = [segue destinationViewController];
        vc.game = selectedGame;
    }
}
@end
