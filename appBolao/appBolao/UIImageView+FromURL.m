//
//  UIImage+FromURL.m
//
//
//  Created by Felix Dumit on 2/3/14.
//
//

#import "UIImageView+FromURL.h"
#import <AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>

@implementation UIImageView(FromURL)

-(void) setImageFromURL:(NSString*) url withPlaceholder:(UIImage*) placeholder
{
    
    
    // create activity indicator to show while loading image
    UIActivityIndicatorView* act = [[UIActivityIndicatorView alloc] initWithFrame:self.frame];
    
    [self addSubview:act];
    
    act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    act.color = [UIColor darkGrayColor];
    
    self.image = [UIImage imageNamed:@"defaultUser"];//placeholder;
    
    
    // create request for image
    AFHTTPRequestOperation *postOperation = [[AFHTTPRequestOperation alloc]
                                             initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    
    postOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [postOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [act stopAnimating];
        
        self.image = responseObject;
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Image error: %@\nRequest:%@", error, [[operation request] URL]);
        [MBProgressHUD hideAllHUDsForView:self animated:YES];
        [act stopAnimating];
    }];
    
    [postOperation start];
    
    [act startAnimating];
    
}


-(void) setImageFromURL:(NSString*) url
{
    
    [self setImageFromURL:url withPlaceholder:Nil];
    
}


@end
