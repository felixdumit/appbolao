//
//  RBRequester.h
//  appBolao
//
//  Created by Felix Dumit on 1/31/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

#define BASE_URL @"http://37.139.17.114:8080"

@interface RBRequester : NSObject


+(RBRequester *)sharedRequester;


-(void) requestWithURL:(NSString*) url withParams:(NSDictionary*) params
            onSuccess:(void(^)(AFHTTPRequestOperation *operation, NSDictionary* responseObject)) successBlock
      onFailureailure:(void(^)(AFHTTPRequestOperation *operation, NSError *error)) failureBlock
         duringRequest:(void(^)()) afterBlock;


-(void) setLogin:(NSString*)user andPassword:(NSString*) password;

//-(void) requestBetOnGameId:(NSString*) gameId withBetValue:(NSInteger)betValue andResult:(NSString*) result;


@end
