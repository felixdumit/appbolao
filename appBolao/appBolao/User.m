//
//  User.m
//  appBolao
//
//  Created by Rafael Padilha on 27/01/14.
//  Copyright (c) 2014 RockBottom. All rights reserved.
//

#import "User.h"
#import <AFNetworking.h>
#import "RBRequester.h"

static User *_sharedInstance = nil;


@implementation User

-(void) updateWithDictionary:(NSDictionary*) dict
{
    
    self.userName = [dict objectForKey:@"username"];
    self.password = [dict objectForKey:@"password"];
    self.betPoints = [[dict objectForKey:@"betpoints"] integerValue];
    self.userId = [dict objectForKey:@"_id"];
    self.totalBetPoints = [[dict objectForKey:@"totalBetPoints"] integerValue];
    self.numBets = [[dict objectForKey:@"totalBets"] integerValue];
    self.totalWinnings = [[dict objectForKey:@"totalWinnings"] integerValue];
    self.betArray = [[NSMutableArray alloc] init];
    
    for(NSDictionary* bet in [dict objectForKey:@"bets"]){
        Bet* b = [[Bet alloc] initWithDictionary:bet];
        b.username = self.userName;
        [self.betArray addObject:b];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedUser" object:self];
    
    
    [User archiveUser];
    
}

// user shared across application
+ (User *)sharedUser
{
    
    if(!_sharedInstance){
        _sharedInstance = [[User alloc] init];
    }
    
    return _sharedInstance;
}


// check if user has bet on game
- (BOOL) hasBetOnGame:(Game*) game{
    for (Bet* bet in self.betArray) {
        if (bet.gameId == game.gameId) return YES;
    }
    return NO;
}

// return bet of a user on a game
-(Bet*) getUserBetOnGame:(Game*) game
{
    
    for (Bet* bet in self.betArray) {
        if ([bet.gameId isEqualToString: game.gameId]) return bet;
    }
    
    return Nil;
    
}


- (void) removeBetOnGame:(Game*) game
{
    
    void (^successBlock)(AFHTTPRequestOperation *operation, NSDictionary* responseObject) = ^void(AFHTTPRequestOperation *operation, NSDictionary* json){
        
        // update game and user
        [self updateWithDictionary:[json objectForKey:@"user"]];
        [game updateWithDictionary:[json objectForKey:@"game"]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeBetInGame" object:self];
        
        
    };
    
    void (^errorBlock)(AFHTTPRequestOperation *operation, NSError* error) = ^void(AFHTTPRequestOperation *operation, NSError* error){
        NSLog(@"Error: %@", error);
    };
    
    
    [[RBRequester sharedRequester] requestWithURL:@"/users/removeBet"
                                       withParams:@{@"gameId": game.gameId}
                                        onSuccess:successBlock
                                  onFailureailure:errorBlock
                                    duringRequest:Nil];
    
}

-(void) betInGame:(Game*) game withBet:(NSInteger) betValue onTeam:(NSString*) betTeam
{
    
    void (^successBlock)(AFHTTPRequestOperation *operation, NSDictionary* responseObject) = ^void(AFHTTPRequestOperation *operation, NSDictionary* json){
        
        // update game and user
        [self updateWithDictionary:[json objectForKey:@"user"]];
        [game updateWithDictionary:[json objectForKey:@"game"]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"addedBetInGame" object:self];
        
        
    };
    
    void (^errorBlock)(AFHTTPRequestOperation *operation, NSError* error) = ^void(AFHTTPRequestOperation *operation, NSError* error){
        NSLog(@"Error: %@", error);
    };
    
    [[RBRequester sharedRequester] requestWithURL:@"/users/bet"
                                       withParams:@{@"gameId": game.gameId, @"betValue": [NSNumber numberWithInteger:betValue], @"result": betTeam}
                                        onSuccess:successBlock
                                  onFailureailure:errorBlock
                                    duringRequest:Nil];
    
    
    
}


// calculate winning given a bet on a game
-(NSInteger) predictWinningOfBet:(Bet*) bet onGame:(Game*)game
{
    NSInteger total = game.totalPoints + bet.betValue;
    NSInteger correct = bet.betValue;
    
    // adjust if user already has a bet on game
    Bet* previousBet = [self getUserBetOnGame:game];
    if(previousBet){
        total -= previousBet.betValue;
        if([previousBet.predict isEqualToString:bet.predict]){
            correct -= previousBet.betValue;
        }
    }
    
    if([bet.predict isEqualToString: @"H"]) correct += game.totalH;
    else if([bet.predict isEqualToString:@"A"]) correct += game.totalA;
    else if([bet.predict isEqualToString:@"D"]) correct += game.totalD;
    
    
    return ceil(total * 1.0 * bet.betValue / MAX(1,MAX(correct,bet.betValue)));
}

#pragma mark - NSCoding Protocol

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.userName forKey:@"userName"];
    [encoder encodeObject:self.password forKey:@"password"];
    [encoder encodeInteger:self.betPoints forKey:@"betPoints"];
    [encoder encodeObject:self.betArray forKey:@"betArray"];
    [encoder encodeInteger:self.totalBetPoints forKey:@"totalBetPoints"];
    [encoder encodeInteger:self.numBets forKey:@"numBets"];
    [encoder encodeInteger:self.totalWinnings forKey:@"totalWinnings"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if(self = [super init]){
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.userName = [decoder decodeObjectForKey:@"userName"];
        self.password = [decoder decodeObjectForKey:@"password"];
        self.betPoints = [decoder decodeIntegerForKey:@"betPoints"];
        self.betArray = [decoder decodeObjectForKey:@"betArray"];
        self.totalBetPoints = [decoder decodeIntegerForKey:@"totalBetPoints"];
        self.numBets = [decoder decodeIntegerForKey:@"numBets"];
        self.totalWinnings = [decoder decodeIntegerForKey:@"totalWinnings"];
    }
    return self;
}


+(User*) loadArchivedUser
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+(void) archiveUser
{
    NSData* data = [NSKeyedArchiver  archivedDataWithRootObject:_sharedInstance];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"user"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void) logoutUser
{
    _sharedInstance = nil;
    [User archiveUser];
}


@end
