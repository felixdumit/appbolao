from flask import Flask, jsonify, make_response, request, abort
from pymongo import Connection
from bson.objectid import ObjectId
from gamesAPI import games_api
from usersAPI import users_api,getUserStats
from offlineDB import *
from threadManager import ThreadManager



from flask.ext.httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()

app = Flask(__name__)
app.register_blueprint(games_api, url_prefix= '/games')
app.register_blueprint(users_api, url_prefix= '/users')


app.db = Connection('mongodb://debugger:debugger@troup.mongohq.com:10067/Bolao').Bolao



# db.games.remove()
# for x in games:
#     db.games.save(x)

# db.users.remove()
# for x in users:
#      db.users.save(x)

@app.route("/pholors")
def pholors():
    return "Welcome to Pholors"

@app.route("/")
@auth.login_required
def hello():
    return "Hello World Reloaded!"

@app.route("/encode/<text>")
def encode64(text):
    return text.encode('base64')

@app.route("/decode/<text>")
def decode64(text):
    return text.decode('base64')

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({ 'error': 'Not found' }), 404)

@auth.get_password
def get_password(username):
    user = app.db.users.find_one({'username': username})
    if user:
        app.user = user
        return user.get('password', None)
    return None

@auth.error_handler
def unauthorized():
    return make_response(jsonify( { 'error': 'Unauthorized access' } ), 401)


# @app.route('/reload')
# def reload():
#     exit(1)


@app.route('/login')
@auth.login_required
def userLogin():
    #username = request.json.get('username')
    #password = request.json.get('password')
    return jsonify(getUserStats(app.user))


@auth.verify_password
def verify_password(username, password):
    user = app.db.users.find_one({'username':username})
    if user and 'error' not in user:
        if user['password'] == password:
            app.user = user
            return True
        else:
            return False
    else:
        app.user = {'username':username, 'password': password, 'betpoints':100, 'bets':[]}
        app.db.users.save(app.user)
        return True



import json

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

app.json_encoder = JSONEncoder

if __name__ == "__main__":
    #fire(app)
    ThreadManager(app)





