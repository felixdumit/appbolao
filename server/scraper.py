from bs4 import BeautifulSoup
import requests
from pymongo import Connection
from datetime import datetime
import pytz



def loadGames(db, url):

	r = requests.get(url)
	soup = BeautifulSoup(r.text)

	div = soup.find('table', {'class': 'tab'})

	rows = div.findAll('tr')

	#print rows[3].find('td').get('class')

	games = []

	for row in rows:
		tds = row.findAll('td')
		if tds[0].get('class') == ['nationf']:
			lastleagueImage = 'http://www.soccervista.com/flags/%s.gif' % tds[0].find('span').get('class')[1]
			lastleague = tds[1].text
		elif tds[0].get('class') == ['nocol']:
			game = {
					'time' : tds[0].text.strip(),
					'status': tds[1].text.strip(),
					'Home' : unicode(tds[2].text.strip()),
					'score' : tds[3].text.strip(),
					'Away' : unicode(tds[4].text.strip()),
					'gavePrizes': 0,
					'League' : unicode(lastleague),
					'LeagueImg': lastleagueImage,
					'date' : datetime.strftime(datetime.now(pytz.utc), "%d/%m"),
					'bets' : []
			}

			score = game['score'].split(':')
			try:
				if int(score[0]) > int(score[1]):
					game['result'] = 'H'
				elif int(score[0]) < int(score[1]):
					game['result'] = 'A'
				elif int(score[0]) == int(score[1]):
					game['result'] = 'D'
			except ValueError:
				game['result'] = ''

			updateQuery = {k: game.get(k, None) for k in ('time', 'Home', 'Away', 'date')}
			db.games.update(updateQuery,{'$set':game}, upsert=True)


db = Connection('mongodb://debugger:debugger@troup.mongohq.com:10067/Bolao').Bolao
db.games.ensure_index([ ('Home', 1), ('Away', 1), ('date',1), ('time',1) ],unique=True)

#db.games.remove()
loadGames(db, 'http://www.livescore.cz/index.php')
#loadGames(db, 'http://www.livescore.cz/yesterday.php')
