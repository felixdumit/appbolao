from flask import Flask, jsonify, make_response, request, abort, Blueprint, current_app
from offlineDB import users
from bson.objectid import ObjectId
from flask.ext.httpauth import HTTPBasicAuth
from helper import getWinRatios



users_api = Blueprint('users_api', __name__)
auth = HTTPBasicAuth()


# @users_api.route("/simulatebet", methods=['GET'])
# @auth.login_required
# def simulateBetOnGame():
# 	gameId = request.args.get('gameId')
# 	bet = int(request.args.get('betValue'))
# 	result = request.args.get('result')

# 	if not (gameId and bet and result):
# 		return jsonify({'error': 'invalid request, please supply betValue and result'}), 400

# 	user = current_app.user
# 	if user['betpoints'] < bet:
# 		return jsonify({'result': 'insuficient funds'})
# 	if result not in ['H', 'A', 'D']:
# 		return jsonify({'error': 'invalid result, please pick (H, A, D)'})
# 	game = current_app.db.games.find_one({'_id': ObjectId(gameId)})

# 	return jsonify(getWinRatio(result,game))


#example http://localhost:8080/users/bet?gameId=52e936515cb91803c526b42c&betValue=20&result=A
@users_api.route("/bet", methods=['GET'])
@auth.login_required
def userBetOnGame():
	gameId = request.args.get('gameId')
	betValue = int(request.args.get('betValue',0))
	result = request.args.get('result')
	if not (gameId and betValue and result):
		return jsonify({'error': 'invalid request, please supply betValue and result'}), 400

	user = current_app.user
	# check that user has enough points 
	if user['betpoints'] < betValue:
		return jsonify({'error': 'insuficient betpoints'})

	if betValue <= 0:
		return jsonify({'error': 'bet value must be greater than zero'})

	if result not in ['H', 'A', 'D']:
		return jsonify({'error': 'invalid result, please pick (H, A, D)'})

	try:
		game = current_app.db.games.find_one({'_id': ObjectId(gameId)})
	except:
		return jsonify({'error': 'invalid game id'})

	# check that game is still not finished
	if not game:
		return jsonify({'error': 'game doesnt exist'})
	if game['result'] != '':
		return jsonify({'error': 'game already started, cant bet'})

	user['betpoints'] -= betValue

	# if there already is a bet from that user in that game, update value
	userBet = {
				'gameId': game['_id'],
				'betValue': betValue,
				'predict': result,
				'completed' : 0,
				}
	
	found =0
	for bet in user.get('bets', []):
		if bet['gameId'] == game['_id']:
			user['betpoints'] += bet['betValue']
			bet.update(userBet)
			found =1
	if not found:
		user.get('bets',[]).append(userBet)


	gameBet = {
				'user': user['username'],
		       	'betValue': betValue,
		       	'predict': result
				}

	found=0
	for bet in game.get('bets', []):
		if bet['user'] == user['username']:
			bet.update(gameBet)
			found=1
	if not found:
		game.get('bets',[]).append(gameBet)
	
	current_app.db.users.save(user)
	current_app.db.games.save(game)

	game.update(getWinRatios(game))

	return jsonify({'user':getUserStats(user), 'game':game})



@users_api.route("/removeBet", methods=['GET'])
@auth.login_required
def removeBet():
	gameId = request.args.get('gameId')
	if not gameId:
		return jsonify({'error': 'please supply gameId'})

	user = current_app.user

	print user['bets'][0]['gameId'], gameId

	try:
		game = current_app.db.games.find_one({'_id': ObjectId(gameId)})
	except:
		return jsonify({'error': 'invalid game id'})

	if not game:
		return jsonify({'error': 'game doesnt exist'})

	userBet = [x for x in game['bets'] if x['user'] == user['username']]
	if not userBet:
		return jsonify({'error': 'user hasnt bet on specified game'})

	game['bets'] = [x for x in game['bets'] if x['user'] != user['username']]
	user['bets'] = [x for x in user['bets'] if str(x['gameId']) != str(gameId)]
	user['betpoints'] += userBet[0]['betValue']
	
	current_app.db.games.save(game)
	current_app.db.users.save(user)

	game.update(getWinRatios(game))

	return jsonify({'user':getUserStats(user), 'game':game})






@users_api.route("/<username>", methods=['GET'])
def getUserL(username):
	user = current_app.db.users.find_one({'username':username})
	if not user:
		return jsonify({'error': 'non existing user'})
	return jsonify(getUserStats(user))

def getUserStats(user):

	user['totalBets'] = len(user['bets'])
	user['totalBetPoints'] = int(sum([x['betValue'] for x in user['bets']]))
	user['totalWinnings'] = int(sum([x.get('winnings',0) for x in user['bets']]))
	user['betpoints'] = int(user['betpoints'])

	return user


@users_api.route("/gameBets")
@auth.login_required
def getUserGameBets():
	user = current_app.user

	gameIdList = []
	for bet in user['bets']:
		gameIdList.append(bet['gameId'])

	gamesList = [game for game in current_app.db.games.find({'_id': {'$in':gameIdList}})]
	for game in gamesList:
		game.update(getWinRatios(game)) 

	return jsonify({'games': gamesList})



@users_api.route("/", methods=['GET'])
def showUsers():
	users = [x for x in current_app.db.users.find()]
	return jsonify({'users':users})
# @auth.get_password
# def get_password(username):
#     user = current_app.db.users.find_one({'username': username})
#     if user:
#         return user.get('password', None)
#     return None

@auth.verify_password
def verify_password(username, password):
    user = current_app.db.users.find_one({'username':username})
    if not user or not user['password'] == password:
        return False
    current_app.user = user
    return True

