from pymongo import Connection
from bson.objectid import ObjectId
from math import ceil
from datetime import date, timedelta, datetime
import pytz



#db = Connection('mongodb://debugger:debugger@troup.mongohq.com:10067/Bolao').Bolao

def getDateForDay(day): 
    if day == 'yesterday':
        date = datetime.strftime(datetime.now(pytz.utc) - timedelta(1), "20%y-%m-%d")
    elif day == 'tomorrow':
        date = datetime.strftime(datetime.now(pytz.utc) + timedelta(1), "20%y-%m-%d") 
    else: #if day == 'today':
        date = datetime.strftime(datetime.now(pytz.utc), "20%y-%m-%d")  

    return date

def getWinRatios(game):
    return calculateGameResults(game)
#   return {'ratio': calculateWonPoints(1,total, correct), 'totalPoints': totalPoints, 'sameBetPoints'}


def calculateGameResults(game):
    ret = {'numBets': len(game.get('bets',[]))}

    ret['total'] = sum(item['betValue'] for item in game.get('bets',[]))
    for result in ['H', 'A', 'D']:
        ret['total%s'%result] =  sum(item['betValue'] for item in game.get('bets',[]) if item['predict'] == result)
        ret['ratio%s'%result] = (1.0 * max(1,ret['total']) / max(1,ret['total%s'%result]))
    
    return ret

def distributePointsForGame(db,game):
    if game['result'] != '' and game['gavePrizes'] == 0 and game['status'] == 'Full time':
        
        stats = calculateGameResults(game)
        
        for bet in game['bets']: # loop through winners
            user = db.users.find_one({'username': bet['user']})
            
            if bet['predict'] == game['result']:
                wonPoints = calculateWonPoints(bet['betValue'], stats['total'], stats['total%s' % game['result']])
            else:
                wonPoints = 0

            for ubet in user['bets']:
                if ubet['gameId'] == game['_id']:
                    ubet['completed'] = 1
                    ubet['winnings'] = int(wonPoints - ubet['betValue'])
                    user['betpoints'] += wonPoints

            db.users.save(user)

        game['gavePrizes'] = 1
        db.games.save(game)
        return True
    return False

        
def calculateWonPoints(bet, total, correct):
    return ceil(1.0 * total * bet/ max(1,correct, bet))








#print setUserBetOnGame('felix', '52e936515cb91803c526b42d', 40, 'D')
#print setUserBetOnGame('rafa', '52e936515cb91803c526b42d', 40, 'A')



#print distributePointsForGame('52e936515cb91803c526b42d')



#for game in [db.games.find({})]