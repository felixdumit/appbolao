from flask import Flask, jsonify, make_response, request, abort, Blueprint, current_app, redirect, url_for
from offlineDB import games
from bson.objectid import ObjectId
from helper import getWinRatios, getDateForDay
from datetime import datetime, timedelta
import pytz


games_api = Blueprint('games_api', __name__)


@games_api.route('/search')
def gamesList(x=0):
    day = request.args.get('day')
    finished = int(request.args.get('finished', -1))

    json = {}

    json.update({'date': getDateForDay(day)})
    if finished == 0 or finished == 1:
        json.update({'gavePrizes' : finished})
    gamelist = [x for x in current_app.db.games.find(json)]
    

    json = {}

    leagues = set(x['League'] for x in gamelist)
    for league in leagues:
        json[league] = []
    for game in gamelist:
        game.update(getWinRatios(game))
        json[game['League']].append(game)

    return jsonify(json)

@games_api.route('/finished')
def finishedGamesList():
    return gamesList(1)

@games_api.route('/all')
def allGames():
    json = {}

    gamelist = [x for x in current_app.db.games.find()]
    leagues = set(x['League'] for x in gamelist)
    for league in leagues:
        json[league] = []
    for game in gamelist:
        game.update(getWinRatios(game))
        json[game['League']].append(game)

    return jsonify(json)

@games_api.route('/<gameId>', methods=['GET'])
def getGame(gameId):
    game = current_app.db.games.find_one({'_id':ObjectId(gameId)})
    if not game:
        return jsonify({'error': 'game not found'})
    
    #game['totalpoints'], _, game['totalbets'] = calculateGameResults(game,'')
    game.update(getWinRatios(game))
   
    return jsonify(game)


@games_api.route('/<gameId>/clearBets', methods=['GET'])
def clearGameBets(gameId):
    current_app.db.games.update({'_id':ObjectId(gameId)}, {'$set': {'bets': []}})

    current_app.db.users.update({'bets': {'$elemMatch':{'gameId':ObjectId(gameId)}}},
                {'$pull': {'bets': {'gameId':ObjectId(gameId)} }}, 
                upsert=False, multi=True)

    return jsonify({'result': 'cleared bets'})


@games_api.route('/<int:gameId>', methods=['POST'])
def createGame(gameId):
    return 'posting to game %s' % gameId


@games_api.route('/addtest', methods=['POST'])
#@auth.login_required
def addtest():
    if not request.json or not 'title' in request.json:
        abort(400)
    task = request.json
    print task
    return jsonify( { 'task': task } ), 201

