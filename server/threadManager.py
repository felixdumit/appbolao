import threading
from Queue import Queue
from scraper2 import scrapeGames
from helper import distributePointsForGame
from datetime import date, timedelta, datetime
import pytz
from time import sleep


#

class ThreadManager():
    def __init__(self, app):
        self.app = app
        self.queue = Queue()
        ThreadLoadGames(app.db, self.queue).start()
        ThreadDistributePoints(app.db, self.queue).start()

        self.app.run('0.0.0.0', 8080, debug=True)


class ThreadLoadGames(threading.Thread):
    def __init__(self, db, queue):
        threading.Thread.__init__(self)
        self.db = db
        self.queue = queue
        self.setDaemon(True)

        self.db.games.ensure_index([ ('Home', 1), ('Away', 1), ('date',1) ],unique=True)

  
    def run(self):
      num = 0
      while True:
            if (num % 20) == 1:
                day = 'tomorrow'  
            elif(num% 21) == 2:
                day = 'yesterday'
            else:
                day = 'today'            


            url = 'http://www.goal.com/en/live-scores/fixtures/tab/all/date/%s?ICID=OP'
            print 'loading games for', day
            scrapeGames(self.db, day, url, self.queue)

            print 'scraped games'
            #sleep 5 mins
            sleep(5 * 60)
            num += 1

  
class ThreadDistributePoints(threading.Thread):
            def __init__(self, db, queue):
                threading.Thread.__init__(self)
                self.db = db
                self.queue = queue
                self.setDaemon(True)
          
            def run(self):
              while True:
                    game = self.queue.get()

                    print 'distributing points for game %s' % game['_id']

                    distributePointsForGame(self.db, game)

                    self.queue.task_done() 

def b():
    print 'b'
