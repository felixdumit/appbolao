from bs4 import BeautifulSoup
import requests
from pymongo import Connection
from datetime import date, timedelta, datetime
import pytz
from helper import getDateForDay


def scrapeGames(db, day, url, queue):
	r = requests.get(url % getDateForDay(day))
	soup = BeautifulSoup(r.text)

	div = soup.find('div', {'id': 'fixtures-tab'})
	rows = div.findAll('div', {'class': ['blackBar', 'matchInfo']})

	for row in rows:
		if row.get('class') == ['blackBar']:
			lastleague = row.find('div').text.encode('ascii','ignore')
			#print lastleague
		elif row.get('class') == ['matchInfo']:
			s = row.find('td', {'class': 'score'}).find('div').get('class')[0]
			time = row.find('td', {'class': 'score'}).text.strip().replace('GMT', ' GMT') if s == 'prematch' else '1PM GMT',
			game = {
					'status': 'Suspended' if s == 'suspended' else row.find('td', {'class': 'time'}).text.strip(),
					'Home' : row.find('a', {'class': 'livesports_hometeam'}).text.strip(),
					'HomeLogo' : row.find('td', {'class': 'homeLogo'}).find('img').get('src'),
					'score' : row.find('td', {'class': 'score'}).text.strip().encode('ascii','ignore') if s in ['played', 'playing','suspended'] else '',
					'Away' : row.find('td', {'class': 'awayTeam'}).text.strip(),
					'AwayLogo' : row.find('td', {'class': 'awayLogo'}).find('img').get('src'),
					#'gavePrizes': 0,
					'League' : lastleague,
					'date' : getDateForDay(day),
					#'bets' : [],
					'matchURl' : row.find('td', {'class': 'score'}).find('a').get('href'),
					'scrapedAt' : datetime.strftime(datetime.now(), "%d/%m %H:%M:%S")#,
					#'homeOdds' :  row.find('td', {'class':'cell_win1'}).text.strip(),
					#'drawOdds' :  row.find('td', {'class':'cell_draw'}).text.strip(),
					#'awayOdds' :  row.find('td', {'class':'cell_win2'}).text.strip()
			}

			score = game['score'].split('-')
			try:
				if int(score[0]) > int(score[1]):
					game['result'] = 'H'
				elif int(score[0]) < int(score[1]):
					game['result'] = 'A'
				elif int(score[0]) == int(score[1]):
					game['result'] = 'D'
			except ValueError:
				game['result'] = ''



			updateQuery = {k: game.get(k, None) for k in ('Home', 'Away', 'date')}
			db.games.update(updateQuery,{'$set':game}, upsert=True)
			gm = db.games.find_one(updateQuery)
			if not gm.get('bets',None):
				gm['bets'] = []
			if not gm.get('time', None):
				gm['time'] = time[0]
			if not gm.get('gavePrizes', None):
				gm['gavePrizes'] = 0

			#trying to fix bug where wrong games are added with time != GMT
			if 'GMT' in gm['time']:
				db.games.save(gm)

			if gm['result'] and gm['status'] == 'Full time' and gm['gavePrizes'] == 0:
				queue.put(gm)
				# distribute points



#db = Connection('mongodb://debugger:debugger@troup.mongohq.com:10067/Bolao').Bolao

#db.games.ensure_index([ ('Home', 1), ('Away', 1), ('date',1), ('time',1) ],unique=True)

#db.games.remove()
#scrapeGames(db, 'http://www.goal.com/en/live-scores/fixtures/tab/all/date/%s?ICID=OP'
#				 % datetime.strftime(datetime.now(pytz.utc) - timedelta(0), "20%y-%m-%d"), None)
#'http://www.goal.com/en/live-scores/fixtures/tab/all?ICID=HP_TN_85')
#loadGames(db, 'http://www.livescore.cz/yesterday.php')
