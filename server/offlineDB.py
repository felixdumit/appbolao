games = \
[
      {
         "id":1,
         "Home":"Man U",
         "Away":"Man City",
         "League": "BPL",
         "Date":"09/02/2014 8h00",
         "score":"1x1",
         "result":"D",
         "bets":[

         ]
      },
      {
         "id":2,
         "Home":"Liverpool",
         "Away":"Chelsea",
         "League":"BPL",
         "Date":"09/02/2014 10h00",
         "score":"3x1",
         "result":"H",
         "bets":[
            {
               "user":"felix",
               "betValue":20,
               "predict":"A"
            },
            {
               "user":"rafa",
               "betValue":60,
               "predict":"H"
            }
         ]
      },
      {
         "id":3,
         "Home":"Milan",
         "Away":"Juventus",
         "League": "Serie A",
         "Date":"09/02/2014 8h00",
         "score":"1x2",
         "result":"A",
         "bets":[
            {
               "user":"felix",
               "betValue":30,
               "predict":"H"
            }
         ]
      }
   
]


users = \
[
   {
      "username":"felix",
      "password":"debugger",
      "betpoints":150,
      "bets":[
         {
            "gameId":2,
            "betValue":20,
            "predict":"A"
         },
         {
            "gameId":3,
            "betValue":30,
            "predict":"H"
         }
      ]
   },
   {
      "username":"rafa",
      "password":"debugger",
      "betpoints":90,
      "bets":[
         {
            "gameId":2,
            "betValue":60,
            "predict":"H"
         }
      ]
   }
]



## result = H, A, D (home away draw)